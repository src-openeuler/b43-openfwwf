Name:		b43-openfwwf
Version:	5.2
Release:	23
Summary:	Open Source firmware for Broadcom/AirForce chipset based devices
License:	GPLv2
URL:		http://www.ing.unibs.it/openfwwf/
Source0:	http://netweb.ing.unibs.it/openfwwf/firmware/openfwwf-%{version}.tar.gz
Source1:	README.openfwwf
Source2:	openfwwf.conf
BuildArch:	noarch

BuildRequires:	b43-tools gcc-c++
Requires:	udev module-init-tools

%description
Open Source firmware for Broadcom/AirForce chipset based devices.
Known supported boards until now: 4306, 4311(rev1), 4318 and 4320.

%package_help

%prep
%autosetup -n  openfwwf-%{version}
sed -i s/"-o 0 -g 0"// Makefile
install -p -m 0644 %{SOURCE1} .
install -p -m 0644 %{SOURCE2} .

%build
%make_build

%install
make install PREFIX=$RPM_BUILD_ROOT/lib/firmware/b43-open
install -d  $RPM_BUILD_ROOT%{_prefix}/lib/modprobe.d
install -p  openfwwf.conf $RPM_BUILD_ROOT%{_prefix}/lib/modprobe.d/openfwwf.conf

%files
%license COPYING LICENSE
/lib/firmware/b43-open/*.fw
%attr(0644,root,root) %{_prefix}/lib/modprobe.d/openfwwf.conf

%files help
%doc README.openfwwf

%changelog
* Sat Nov 26 2022 wangye <wangye91@h-partners.com> - 5.2-23
- fix source url problem

* Thu Sep 10 2020 hanzhijun <hanzhijun1@huawei.com> - 5.2-22
- fix source url problem

* Fri Oct 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.2-21
- Package init


